<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>AlipayPro</title>
</head>
<?php

echo '<pre>';

$dirname = str_replace('modules/alipaypro', '', dirname($_SERVER['SCRIPT_FILENAME']));

include_once($dirname.'/config/config.inc.php');
include_once($dirname.'/init.php');

include_once(_PS_MODULE_DIR_.'alipaypro/api/lib/alipay_submit.class.php');

$alipay_config = array(
    'partner' => Configuration::get('ALIPAYPRO_PARTNER_ID'),
    'key' => Configuration::get('ALIPAYPRO_API_SECRET'),
    'sign_type' => 'MD5',
    'input_charset' => Configuration::get('ALIPAYPRO_INPUT_CHARSET'),
    'cacert' => Configuration::get('ALIPAYPRO_SSL_CERT'),
    'transport' => 'http',
    'seller_email' => Configuration::get('ALIPAYPRO_SELLER_EMAIL')
);

// $api = new Alipay();
// $api->config->endpoint(Configuration::get('ALIPAYPRO_ENDPOINT'));
// $api->config->partner_id(Configuration::get('ALIPAYPRO_PARTNER_ID'));
// $api->config->seller_email(Configuration::get('ALIPAYPRO_SELLER_EMAIL'));
// $api->config->charset(Configuration::get('ALIPAYPRO_INPUT_CHARSET'));
// $api->config->secret(Configuration::get('ALIPAYPRO_API_SECRET'));
// $api->config->ssl_cert(Configuration::get('ALIPAYPRO_SSL_CERT'));

$id_cart = (int)Tools::getValue('id_cart');

$cart = new Cart($id_cart);

$id_module = (int)Tools::getValue('id_module');
$customer = new Customer($cart->id_customer);
$currency = new Currency($cart->id_currency);

if ($currency->iso_code !== 'CNY') {
    exit('Sorry alipay only supports Chinese Yuan currency (CNY)');
}

$return_url = (Configuration::get('PS_SSL_ENABLED') ? 'https' : 'http').
    '://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__.
    'order-confirmation.php?id_cart='.$id_cart.
    '&id_module='.$id_module.'&key='.$customer->secure_key;

$show_url = (Configuration::get('PS_SSL_ENABLED') ? 'https' : 'http').
    '://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

$notify_url = 'http://www.arts-facts.net/alipay/notify.php';

$amount = '0.01';

$description = '';

$parameter = array(
        "service" => "create_direct_pay_by_user",
        "partner" => trim($alipay_config['partner']),
        "payment_type"  => '1',
        "notify_url"    => $notify_url,
        "return_url"    => $return_url,
        "seller_email"  => $alipay_config['seller_email'],
        "out_trade_no"  => $id_cart,
        "subject"   => Configuration::get('PS_SHOP_NAME'),
        "total_fee" => $amount,
        "body"  => $description,
        "show_url"  => $show_url,
        // "anti_phishing_key" => $anti_phishing_key,
        // "exter_invoke_ip"   => $exter_invoke_ip,
        "_input_charset"    => trim(strtolower($alipay_config['input_charset']))
);

//建立请求
$alipaySubmit = new AlipaySubmit($alipay_config);
$html_text = $alipaySubmit->buildRequestForm($parameter,"get", "确认");
echo $html_text;

//$alipayUrl = $api->createPayment($id_cart, $amount, $description, $return_url, $notify_url);

//header('Location: ' . $alipayUrl);

?>
</body>
</html>
