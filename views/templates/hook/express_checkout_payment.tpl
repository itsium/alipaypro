{*
* 2014 Itsium Inc.
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Itsium Inc. <contact@itsium.cn>
*  @copyright  2014 Itsium Inc.
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of Itsium Inc.
*}

<p class="payment_module">
    <a href="javascript:void(0);" onclick="$('#alipaypro_payment_form').submit();" id="paypal_process_payment" title="{l s='Pay with Alipay' mod='alipaypro'}">
        <img src="{$base_dir_ssl}modules/alipaypro/images/logo_payment.png" />
        {l s='Pay with Alipay'}
    </a>
</p>

<form id="alipaypro_payment_form" action="{$base_dir_ssl}modules/alipaypro/payment.php" data-ajax="false" title="{l s='Pay with Alipay' mod='alipaypro'}" method="post">
    <input type="hidden" name="id_cart" value="{$AlipayPro_id_cart|escape:'htmlall':'UTF-8'}"/>
    <input type="hidden" name="id_module" value="{$AlipayPro_id_module|escape:'htmlall':'UTF-8'}" />
</form>
