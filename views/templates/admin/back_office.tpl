{*
* 2014 Itsium Inc.
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Itsium Inc. <contact@itsium.cn>
*  @copyright  2014 Itsium Inc.
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of Itsium Inc.
*}

<div id="alipaypro-wrapper">

    {* Alipay Pro configuration page header *}

    {if empty($AlipayPro_html) === false}
        {$AlipayPro_html}
    {/if}

    <h2>{$AlipayPro_self->displayName}</h2>
    <form action="{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}" method="post">
        <input type="hidden" name="submitAlipayPro" value="alipaypro_configuration" />
        <fieldset>
            <p>
                <label for="seller_email">{l s='Alipay Email Account'}</label>
                <input type="text" id="seller_email" name="seller_email" value="{$AlipayPro_seller_email}" placeholder="myalipayaccount@mail.com" style="width: 250px;" />
            </p>
            <p>
                <label for="partner_id">{l s='Alipay Partner ID'}</label>
                <input type="text" id="partner_id" name="partner_id" value="{$AlipayPro_partner_id}" placeholder="2088123456789012" style="width: 250px;" />
            </p>
            <p>
                <label for="api_secret">{l s='API Secret'}</label>
                <input type="text" id="api_secret" name="api_secret" value="{$AlipayPro_api_secret}" placeholder="Your api secret key" style="width: 250px;" />
            </p>
            <div class="margin-form">
                <input type="submit" name="submitModule" value="{l s='Update settings'}" class="button" /></center>
            </div>
        </fieldset>
    </form>

</div>
