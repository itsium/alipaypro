<?php
/*
* 2014 Itsium Inc.
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Itsium Inc. <contact@itsium.cn>
*  @copyright  2014 Itsium Inc.
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of Itsium Inc.
*/

if (!defined('_PS_VERSION_'))
    exit;

class AlipayPro extends PaymentModule
{

    public $_errors = array();

    protected $_html = '';

    public function __construct()
    {
        $this->name = 'alipaypro';
        $this->tab = 'payments_gateways';
        $this->version = '1.0';
        $this->author = 'Itsium.cn';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.3', 'max' => '1.6');
        $this->dependencies = array('blockcart');

        parent::__construct();

        $this->displayName = $this->l('Alipay Pro 支付宝');
        $this->description = $this->l('Accepts payments by credit cards (Unionpay, CB, Visa, MasterCard) with Alipay 支付宝 from Chinese customers.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall Alipay Pro 支付宝 ?');

        if (!Configuration::get('ALIPAY_PRO_HELLOWORLD'))
            $this->warning = $this->l('No name provided');
    }

    public function install()
    {
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        return parent::install() &&
            $this->registerHook('payment') &&
            $this->registerHook('paymentReturn') &&
            Configuration::updateValue('ALIPAYPRO_PARTNER_ID', '') &&
            Configuration::updateValue('ALIPAYPRO_API_SECRET', '') &&
            Configuration::updateValue('ALIPAYPRO_SELLER_EMAIL', '') &&
            Configuration::updateValue('ALIPAYPRO_INPUT_CHARSET', 'utf-8') &&
            Configuration::updateValue('ALIPAYPRO_ENDPOINT', 'https://mapi.alipay.com/gateway.do') &&
            Configuration::updateValue('ALIPAYPRO_SSL_CERT', '/api/alipay_ssl_cert.pem');
    }

    public function uninstall()
    {
        // if (!parent::uninstall())
        //   Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'mymodule`');
        // parent::uninstall();

        return parent::uninstall() &&
            Configuration::deleteByName('ALIPAYPRO_PARTNER_ID') &&
            Configuration::deleteByName('ALIPAYPRO_API_SECRET') &&
            Configuration::deleteByName('ALIPAYPRO_SELLER_EMAIL') &&
            Configuration::deleteByName('ALIPAYPRO_INPUT_CHARSET') &&
            Configuration::deleteByName('ALIPAYPRO_ENDPOINT') &&
            Configuration::deleteByName('ALIPAYPRO_SSL_CERT');
    }

    public function hookPayment($params)
    {
        if (!$this->active)
            return;

        $allowedCurrencies = $this->getCurrency((int)$params['cart']->id_currency);
        $currency = new Currency((int)$params['cart']->id_currency);

        if ($currency->iso_code !== 'CNY') {
            return false;
        }

        //$this->getTranslations();
        $this->context->smarty->assign(array(
            'AlipayPro_id_cart' => (int)$params['cart']->id,
            'AlipayPro_id_module' => (int)$this->id));

        return $this->fetchTemplate('express_checkout_payment.tpl');
    }

    public function hookPaymentReturn($params)
    {
        if (!$this->active)
            return ;

        global $smarty;

        switch($params['objOrder']->getCurrentState())
        {
            case _PS_OS_PAYMENT_:
                $smarty->assign('status', 'ok');
                break;
            case _PS_OS_OUTOFSTOCK_:
                $smarty->assign('status', 'ok');
                break;

            case _PS_OS_BANKWIRE_:
                $smarty->assign('status', 'pending');
                break;

            case _PS_OS_ERROR_:
            default:
                $smarty->assign('status', 'failed');
                break;
        }

        return $this->display(__FILE__, 'confirmation.tpl');
    }

    public function getContent()
    {

        $this->__postProcess();

        $this->context->smarty->assign(array(
            'AlipayPro_self' => $this,
            'AlipayPro_partner_id' => (int)Configuration::get('ALIPAYPRO_PARTNER_ID'),
            'AlipayPro_api_secret' => Configuration::get('ALIPAYPRO_API_SECRET'),
            'AlipayPro_seller_email' => Configuration::get('ALIPAYPRO_SELLER_EMAIL'),
            'AlipayPro_html' => $this->_html));

        $output = $this->fetchTemplate('/views/templates/admin/back_office.tpl');

        return $output;
    }

    private function __postProcess() {

        if (Tools::isSubmit('submitAlipayPro'))
        {

            if ($this->__preProcess())
            {
                Configuration::updateValue('ALIPAYPRO_PARTNER_ID', (int)Tools::getValue('partner_id'));
                Configuration::updateValue('ALIPAYPRO_API_SECRET', trim(Tools::getValue('api_secret')));
                Configuration::updateValue('ALIPAYPRO_SELLER_EMAIL', trim(Tools::getValue('seller_email')));
            }
            else
            {
                $this->_html = $this->displayError(implode('<br />', $this->_errors)); // Not displayed at this time
                //$this->context->smarty->assign('AlipayPro_save_failure', true);
            }
        }

    }

    private function __preProcess()
    {
        if (Tools::isSubmit('submitAlipayPro'))
        {
            $partner_id = Tools::getValue('partner_id') !== false ? (int)Tools::getValue('partner_id') : false;
            $api_secret = Tools::getValue('api_secret') !== false ? trim(Tools::getValue('api_secret')) : false;
            $seller_email = Tools::getValue('seller_email') !== false ? trim(Tools::getValue('seller_email')) : false;

            if (empty($partner_id) || empty($api_secret) || empty($seller_email))
                $this->_errors[] = $this->l('Some fields are empty.');
        }

        return !count($this->_errors);
    }

    public function fetchTemplate($name)
    {
        if (version_compare(_PS_VERSION_, '1.4', '<')) {
            $this->context->smarty->currentTemplate = $name;
        } elseif (version_compare(_PS_VERSION_, '1.5', '>=')) {

            $views = 'views/templates/';
            $dirname = dirname(__FILE__);

            if (file_exists($dirname.'/'.$name)) {
                return $this->display(__FILE__, $name);
            } else if (file_exists($dirname.'/'.$views.'hook/'.$name)) {
                return $this->display(__FILE__, $views.'hook/'.$name);
            } else if (file_exists($dirname.'/'.$views.'front/'.$name)) {
                return $this->display(__FILE__, $views.'front/'.$name);
            } else if (file_exists($dirname.'/'.$views.'admin/'.$name)) {
                return $this->display(__FILE__, $views.'admin/'.$name);
            }
        }
        return $this->display(__FILE__, $name);
    }
}

?>
